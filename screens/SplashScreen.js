import React,{useState, useEffect } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    Dimensions,
    StyleSheet,
    StatusBar,
    Image,
    FlatList,
    Modal,
    
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useTheme } from '@react-navigation/native';
import { Rating, AirbnbRating } from 'react-native-elements';
import FlatListSlider from './src/FlatListSlider';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Card ,Avatar,Input } from 'react-native-elements';
import Preview from './src/Preview';
import { Button } from 'react-native-paper';
const size=30


const SplashScreen = ({navigation}) => {
    const { colors } = useTheme();
    // const data = [
    //   { id: '1', employee_name: 'First item' },
    //   { id: '2', employee_name: 'Second item' },
    //   { id: '3', employee_name: 'Third item' },
    //   { id: '4', employee_name: 'Fourth item' }
    // ];
    const [data, setData] = useState([]);
    const [isVisible,setVisible]=useState(false)
    const [error, setError] = useState(null);
    const [age, setage] = useState(null);
    const [name, setName] = useState(null);
    const [salary, setSalary] = useState(null);
    const [id, setId] = useState(null);
    const [userData, setUserData] = useState();
    // useEffect();
    useEffect(() => {
      console.log('GETDATA')
      axios.get(`http://dummy.restapiexample.com/api/v1/employees`
      )
      .then(function (response) {
        console.log(response.data);
        setData(response.data.data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });
    }, []);
  
    const updateData = (id,name,age,salary) =>{
      console.log('GETDATA')
      axios.post(`http://dummy.restapiexample.com/api/v1/update/${id}`
      )
      .then(function (response) {
        console.log(response.data);
        setData(response.data.data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      }); 
   };
    
     
    // getData();

    return (

        <LinearGradient
        colors={['#ff1493', '#ff1493', '#ba55d3' ]}
        style={styles.container}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
      >
          <StatusBar backgroundColor='#ff1493' barStyle="light-content"/>
        <View style={styles.header}>
        <Text style={styles.text}>Employee List</Text>
   
      <FlatList
        data={data}
        keyExtractor={item => item.id}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
              {/* <Avatar.Image style={{paddingLeft:10}} size={50} source={item.profile_image}/> */}
            <Avatar
            rounded
            size={100}
              source ={{
                  uri:
                    'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
                }}         
               style={styles.coverImage}
            />
            <View style={styles.metaInfo}>
            <Text>{`id                     : ${item.id}`}</Text>
              <Text>{`Name              : ${item.employee_name}`}</Text>
              <Text>{`Age                 : ${item.employee_age}`}</Text>
              <Text>{`Emp Salary    : ${item.employee_salary}`}</Text>
              <View style={styles.button1}>
            <TouchableOpacity onPress={()=>
            // getData()
            {
              console.log('item',item);
                //  setUserData([item])
                setId(item.id)
                setName(item.employee_name)
                setage(item.employee_age)
                setSalary(item.employee_salary)
                setVisible(true);
            }
            //  navigation.navigate('SignUpScreen')
              
              }>
                <LinearGradient
                      colors={['#ff1493', '#ff1493']}
                    style={styles.signIn1}
                >
                    <Text style={styles.textSign1}>Update</Text>
                    
                </LinearGradient>
            </TouchableOpacity>
            </View>
            </View>
           
          </View>
          
        )}
      />
   
   <Modal  animationType = {"slide"} 
          transparent = {true}
            visible = {isVisible}
            onRequestClose = {() =>{ console.log("") } }
            onBackdropPress={() => {setVisible(false)}
            }
           >

         
          <View style={{ fontSize: 15, backgroundColor:'#e9967a',height:"60%",borderRadius:30,
          width:"90%",margin:15,marginBottom:60,marginTop:150,paddingTop:10,paddingBottom:10,paddingLeft:10,
          paddingRight:10}}> 
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          {/* <Avatar.Image style={{ marginTop:12 , marginLeft:20 }} size={75} source={ Ment_Onnnn}/> */}
          <View style={{width:"90%",
           marginTop:50,
           marginLeft:20,}}>

          <Input
   placeholder="Name"
   backgroundColor='#fff'
   style={styles}
   value={name}
   onChangeText={value => {
         
    setName(name)
    console.log('name',value)
  }
  
  }
  />
      <Input
   placeholder="Age"
   backgroundColor='#fff'
   style={styles}
   value={age}
   onChangeText={value => {
         
    setage(age)
    console.log('age',value)
  }
  
  }
  />
          <Input
   placeholder="Salary"
   backgroundColor='#fff'
   style={styles}
   value={salary}
   onChangeText={value => {
         
    setsalary(salary)
    console.log('salary',value)
  }
  
  }
  />
        
       
              <View>
         
              
              </View>
            
            

                    </View>
                
   
          </View>
<View>


        
<TouchableOpacity onPress={()=>
            // getData()
            {
                setVisible(false)
            }
            //  navigation.navigate('SignUpScreen')
              
              }>
                <LinearGradient
                      colors={['#ff1493', '#ff1493']}
                    style={styles.signIn1}
                >
                    <Text style={styles.textSign1}>Save</Text>
                    
                </LinearGradient>
            </TouchableOpacity>
</View>
     
          </View>




        </Modal>
      
       
</View>


         
         

           
            <View style={styles.button}>
            <TouchableOpacity onPress={()=>
            // getData()
             navigation.navigate('SignInScreen')
              
              }>
                <LinearGradient
                      colors={['#ff1493', '#ff1493']}
                    style={styles.signIn}
                >
                    <Text style={styles.textSign}>Add</Text>
                    
                </LinearGradient>
            </TouchableOpacity>
            </View>
      
      </LinearGradient>
    );
};

export default SplashScreen;

const {height} = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: 'pink',
    marginBottom:30
  },
  coverImage: {
    width: 100,
    height: 100,
   padding:10,
   marginBottom:10,
   paddingBottom:15,
   marginRight:10
  },
  header: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    marginTop:10
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginTop:20,
   
},
listItem: {
  borderRadius:10,
  marginTop: 10,
  paddingVertical: 20,
  paddingHorizontal: 20,
  backgroundColor: '#fff',
  flexDirection: 'row'
},
  logo: {
      width: height_logo,
      height: height_logo
  },
  title: {
      color: '#05375a',
      fontSize: 30,
      fontWeight: 'bold'
  },
  text: {
      color: 'black',
     
  
      fontSize:30,
      fontWeight:"bold"
  },
  button: {
      alignItems: 'flex-end',
      marginTop: 5
  },
  signIn: {
      width: 150,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 50,
      flexDirection: 'row'
  },
  textSign: {
      color: 'white',
      fontWeight: 'bold'
  },
  button1: {
    alignItems: 'flex-start',
    marginTop: 5
},
signIn1: {
    width: 130,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
    marginTop:10
},
textSign1: {
    color: 'white',
    fontWeight: 'bold'
},
  
  card1:{
    backgroundColor:'#fff',
    height:150,
    // borderRadius:20,
    // marginRight:20,
    // margin: 15,
//     marginHorizontal: 10,
//  marginVertical:10,
   borderColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 5,
    elevation: 5,
    borderRadius: 15,
   marginTop:150,
   width:"100%"
},
separator:{
    height:"10%"
},

});
