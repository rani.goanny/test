import React ,{useState}from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    TextInput,
    Platform,
    StyleSheet ,
    StatusBar,
    Alert,
    ScrollView,
    Dimensions,
    Image,
    ActivityIndicator
    
} from 'react-native';
import * as Animatable from 'react-native-animatable';

import LinearGradient from 'react-native-linear-gradient';
import Preview from './src/Preview';
import FlatListSlider from './src/FlatListSlider';
import { Avatar } from 'react-native-paper';
import axios from 'axios';
import { useTheme } from 'react-native-paper';
import { Card ,Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { AuthContext } from '../components/context';
import Home from '../assets/home.png'
import Lunch from '../assets/lunch.png'
import Heart from '../assets/Heart.png'
import Users from '../model/users';
 let _Home=false
const size=30

  const onChange2=(val)=> Alert.alert(
    "Alert Title 2",
    "My Alert Msg",
    [
      {
        text: "Ask me later",
        onPress: () => console.log("Ask me later pressed")
      },
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      { text: "OK", onPress: () => console.log("OK Pressed") }
    ],
    { cancelable: false }
  );



  const SignInScreen = ({navigation}) => {
    const { colors } = useTheme();
   const [_home,setHome]=useState()
   const [name,setName]=useState()
   const [age,setAge]=useState()
   const [salary,setsalary]=useState()
   const [isLoading, setIsLoading] = useState(false);
   
const  data= [
    {
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTivqBmGbK1zZhwUEu42UfSF6B7KXbjeCknkg&usqp=CAU'
    },
    {
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTivqBmGbK1zZhwUEu42UfSF6B7KXbjeCknkg&usqp=CAU'
    },
    {
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT9JsguD9zGnAKQvO9GF0EmoAi-pveiSwQg_Q&usqp=CAU'
    },
    {
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRmIDuDDWhelyj60kKPub6S9HqSSFQodPH-Kg&usqp=CAU'
    },
    {
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT9JsguD9zGnAKQvO9GF0EmoAi-pveiSwQg_Q&usqp=CAU'
    },
  ]
  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color="#5500dc" />
      </View>
    );
  }
  const sendData =()=>{
    setIsLoading(true);
    console.log('SEND DATA',name,age,salary)
        axios.post('http://dummy.restapiexample.com/api/v1/create', {
   
    "employee_name":name,
    "employee_salary":salary,
    "employee_age":age,
    "profile_image":""
      })
      .then(function (response) {
        console.log(response.data);
        setIsLoading(false);
        navigation.navigate('SplashScreen')
      })
      .catch(function (error) {
        console.log(error);
        setIsLoading(false);
      });
      }
  const screenWidth = Math.round(Dimensions.get('window').width);
  const createThreeButtonAlert = () => {
    console.log('::',_Home)
    if(_Home==true){
        _Home=false
    }
   else{
        _Home=true
    }
};

    return (
       
    //   <View style={styles.container}>
           <LinearGradient
           colors={['#ff1493', '#ff1493', '#ba55d3' ]}
        style={styles.container}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
      >
         <Text style={styles.text}>Employee Details</Text>
          <View style={{width:"90%",
           marginTop:50,
           marginLeft:20,}}>


<Input
   placeholder="Name"
   backgroundColor='#fff'
  //  borderRadius="50"
  //  leftIcon={{ type: 'font-awesome', name: 'comment' }}
   style={styles}
   onChangeText={value => {
         
    setName(value)
    console.log('name',value)
  }
  
  }
  />
<Input
   placeholder="Salary"
   backgroundColor='#fff'
   style={styles}
   value={salary}
   onChangeText={value => {
         
    setsalary(value)
    console.log('name',value)
  }
  
  }
  />
<Input
   placeholder="Age"
   backgroundColor='#fff'
   style={styles}
   onChangeText={value => {
         
    setAge(value)
    console.log('name',value)
  }
  
  }
  />


  </View>

     
       
       
  <View style={styles.button}>
            <TouchableOpacity onPress={()=>sendData(name,age,salary)}>
                <View
                 
                    style={styles.signIn}
                >
                    <Text style={styles.textSign}>Add</Text>
                    
                </View>
            </TouchableOpacity>
            </View>


           
        
</LinearGradient>
    );
};

export default SignInScreen;

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#009387',
    },
    button: {
      alignItems: 'center',
      marginTop: 5
  },
  text: {
    color: 'black',
    fontSize:30,
    fontWeight:"bold",
    paddingTop:40,
    marginHorizontal:29
},
  signIn: {
    backgroundColor:'#000',
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    flexDirection: 'row'
},
textSign: {
    color: 'white',
    fontWeight: 'bold'
},
    container1:{ margin: 15,
        marginHorizontal: 10,
        // marginVertical:-100,
        // marginBottom: 17.5,
        // marginTop:-150,
        // borderColor: '#FFFFFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 5,
        borderRadius: 10,
        height:200,
        marginBottom:700,
        marginTop:80
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
    //   paddingBottom:10,
    //      paddingTop:10 ,

    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 10,
        paddingVertical: 30,
        marginTop:30,
       
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    
    
    card1:{
		backgroundColor:'#f5deb3',
        // borderRadius:20,
        // marginRight:20,
        margin: 15,
        marginHorizontal: 10,
     marginVertical:10,
        marginBottom: 17.5,
        paddingTop:-10,
       marginBottom:30,
        borderColor: '#FFFFFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 5,
        borderRadius: 10,
        
    },
    card2:{
		backgroundColor:'#add8e6',
        // borderRadius:20,
        // marginRight:20,
        margin: 15,
        marginHorizontal: 10,
     marginVertical:10,
        marginBottom: 17.5,
        paddingTop:-10,
       marginBottom:30,
        borderColor: '#FFFFFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 5,
        borderRadius: 10
    },
    card3:{
		backgroundColor:'#b0c4de',
        // borderRadius:20,
        // marginRight:20,
        margin: 15,
        marginHorizontal: 10,
     marginVertical:10,
        marginBottom: 17.5,
        paddingTop:-10,
       marginBottom:30,
        borderColor: '#FFFFFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 5,
        borderRadius: 10
    },
    card4:{
		backgroundColor:'#98fb98',
        // borderRadius:20,
        // marginRight:20,
        margin: 15,
        marginHorizontal: 10,
     marginVertical:10,
        marginBottom: 17.5,
        paddingTop:-10,
       marginBottom:30,
        borderColor: '#FFFFFF',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 5,
        elevation: 5,
        borderRadius: 10
    },
   
   
  
   
    btnTxt: {
        fontSize: 13,
        textAlign: 'center',
        margin: 10,
        color: "#000",
    
      },
      userBtn: {
        backgroundColor: "transparent",
        textAlign: 'center',
        borderRadius: 30,
        marginRight: 100,
        marginLeft: 100,
        height:40,
        marginTop:10,
        borderWidth:2,
        borderColor:"pink"
    
      },
  });
